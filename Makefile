PROTO_PATH=protocol
PROTO_SCHEMAS=$(PROTO_PATH)/schemas

proto:
	protoc --proto_path=$(PROTO_SCHEMAS) --go_out=$(PROTO_PATH)/communication $(PROTO_SCHEMAS)/communication/*.proto
