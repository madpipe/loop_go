module gitlab.com/madpipe/loop_go

go 1.13

require (
	github.com/google/gousb v0.0.0-20190812193832-18f4c1d8a750
	google.golang.org/protobuf v1.20.1 // indirect
)
