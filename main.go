package main

import (
	"log"

	"gitlab.com/madpipe/loop_go/device"
)

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	log.Println("Connecting to device...")
	dev, err := device.NewDevice()
	check(err)
	defer dev.Close()
	log.Println("Connected.")

	dev.TestCommunication()

	// log.Println("Fetching FS dump...")
	// fsDump, err := filesystem.Walk("/")
	// check(err)

	// log.Println("Got FS dump:")
	// for _, item := range fsDump {
	// 	fmt.Println(item)
	// }
}
