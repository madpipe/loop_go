package device

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/google/gousb"
)

const (
	vendorID      = 0x0da4
	productID     = 0x0008
	inEnpointNo   = 1
	outEndpointNo = 1
)

// Device represents a Polar Loop Device
// Device.Close() must be called at the end of using the Device
type Device struct {
	usbDevice *gousb.Device
	context   *gousb.Context
	intf      *gousb.Interface
	intfDone  func()
	epIn      *gousb.InEndpoint
	epOut     *gousb.OutEndpoint
}

// NewDevice creates and initializes a Device connection
func NewDevice() (*Device, error) {
	v, p := gousb.ID(vendorID), gousb.ID(productID)
	var err error

	d := Device{}
	d.context = gousb.NewContext()
	d.usbDevice, err = d.context.OpenDeviceWithVIDPID(v, p)
	if err != nil {
		d.context.Close()
		return nil, err
	}
	if d.usbDevice == nil {
		message := fmt.Sprint("Could not connect to the device.")
		d.context.Close()
		return nil, errors.New(message)
	}

	// Do not allow the Kernel to lock the device
	// This lets us comunicate with it
	err = d.usbDevice.SetAutoDetach(true)
	if err != nil {
		message := fmt.Sprintf("%s.SetAutoDetach(): %v", d.usbDevice, err)
		d.context.Close()
		d.usbDevice.Close()
		return nil, errors.New(message)
	}

	d.intf, d.intfDone, err = d.usbDevice.DefaultInterface()
	if err != nil {
		message := fmt.Sprintf("%s.DefaultInterface(): %v", d.usbDevice, err)
		d.context.Close()
		d.usbDevice.Close()
		return nil, errors.New(message)
	}

	d.epIn, err = d.intf.InEndpoint(inEnpointNo)
	if err != nil {
		message := fmt.Sprintf("%s.InEndpoint(1): %v", d.intf, err)
		d.Close()
		return nil, errors.New(message)
	}

	d.epOut, err = d.intf.OutEndpoint(outEndpointNo)
	if err != nil {
		message := fmt.Sprintf("%s.OutEndpoint(1): %v", d.intf, err)
		d.Close()
		return nil, errors.New(message)
	}

	return &d, nil
}

// Close closes all the registered parts of a Device
func (d *Device) Close() {
	d.context.Close()
	d.usbDevice.Close()
	d.intfDone()
}

func (d *Device) queryOnce(p []byte) ([]byte, error) {
	// Create context for timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	bw, err := d.epOut.WriteContext(ctx, p)
	if err != nil {
		return nil, err
	}
	if bw < len(p) {
		message := fmt.Sprintf("Only written %d bytes out of %d",
			bw, len(p))
		return nil, errors.New(message)
	}
	data := make([]byte, d.epIn.Desc.MaxPacketSize)
	_, err = d.epIn.ReadContext(ctx, data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (d *Device) query(p []byte) ([]byte, error) {
	var resp []byte
	var err error

	data, err := d.queryOnce(p)
	if err != nil {
		return nil, err
	}

	for !finishedReading(data) {
		pkgNo := data[2]
		stripPacketCounter(data)
		resp = append(resp, data...)

		req := acknowledgePacket(pkgNo)
		data, err = d.queryOnce(req)
		if err != nil {
			return nil, err
		}
	}

	stripPadding(data)
	stripPacketCounter(data)
	resp = append(resp, data...)

	return resp, nil
}

func finishedReading(p []byte) bool {
	if p[1]&3 == 0 {
		return true
	}
	return false
}

// Create a response packet to acknowledge received package.package device
// Used for acknowledging a packet when a series is to be received
func acknowledgePacket(packetNo byte) []byte {
	packet := []byte{01, 05, packetNo}
	padding := make([]byte, 61)
	packet = append(packet, padding...)
	return packet
}

func stripPacketCounter(p []byte) {
	p = p[3:]
}

// Expects the packet to be full.
// Size to be Device.epIn.Desc.MaxPacketSize
func stripPadding(p []byte) {
	length := p[1] >> 2
	p = p[:length+1]
}

func (d *Device) TestCommunication() {
	check := func(err error) {
		if err != nil {
			log.Fatalln(err)
		}
	}
	var err error
	var bufOut []byte
	bufOut = append(bufOut,
		0x01, 0x94, 0x00, 0x21, 0x00, 0x08, 0x00, 0x12,
		0x1d, 0x2f, 0x55, 0x2f, 0x30, 0x2f, 0x32, 0x30,
		0x31, 0x36, 0x31, 0x31, 0x31, 0x30, 0x2f, 0x41,
		0x43, 0x54, 0x2f, 0x41, 0x53, 0x41, 0x4d, 0x50,
		0x4c, 0x30, 0x2e, 0x42, 0x50, 0x42, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	)

	bufIn, err := d.query(bufOut)

	// log.Println("Writing...")
	// _, err = d.Write(bufOut)
	// check(err)

	// bufIn := make([]byte, d.packetSize())
	// log.Println("Reading...")
	// _, err = d.Read(bufIn)
	check(err)
	log.Println("Bytes read")
	fmt.Println(bufIn)
}
