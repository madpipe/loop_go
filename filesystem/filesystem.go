package filesystem

import (
	"errors"
	"fmt"
	"path"
	"sort"

	"gitlab.com/madpipe/loop_go/device"
)

// Filesystem provides easy access to the Device's filesystem
type Filesystem struct {
	device *device.Device
}

// NewFilesystem creates a new Filesystem object
func NewFilesystem(d *device.Device) *Filesystem {
	fs := Filesystem{}
	fs.device = d

	return &fs
}

func clean(p *string) error {
	if !path.IsAbs(*p) {
		return errors.New("Only absolute paths supported")
	}
	*p = path.Clean(*p)
	return nil
}

// Walk returns a list of all files and directories
// in the provided path. If path is empty it defaults to root.
func (f *Filesystem) Walk(p string) ([]string, error) {
	if err := clean(&p); err != nil {
		return nil, err
	}
	if !f.Exists(p) {
		message := fmt.Sprintf("Path «%s» does not exist", p)
		return nil, errors.New(message)
	}
	if !f.IsDir(p) {
		message := fmt.Sprintf("Only directories can be Walked. %s is a file.", p)
		return nil, errors.New(message)
	}

	var fsDump []string

	items, err := f.ListDir(p)
	if err != nil {
		return nil, err
	}
	for _, item := range items {
		fsDump = append(fsDump, item)
		if f.IsDir(item) {
			items, err := f.Walk(item)
			if err != nil {
				return nil, err
			}
			fsDump = append(fsDump, items...)
		}
	}

	fsDump = append(fsDump, p)
	sort.Strings(fsDump)
	return fsDump, nil
}

// Exists returns true if path exists
func (f *Filesystem) Exists(p string) bool {
	return false
}

// IsDir returns true if the given path is a directory
func (f *Filesystem) IsDir(p string) bool {
	return false
}

// ListDir lists the contents of the directory
func (f *Filesystem) ListDir(p string) ([]string, error) {
	// if err != nil {
	// 	message := fmt.Sprintf("Error reading path «%s»: %s", p, err.Error())
	// 	return nil, errors.New(message)
	// }
	return nil, nil
}
